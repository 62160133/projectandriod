package com.example.sketchgamestore

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModel
import androidx.navigation.findNavController
import com.example.sketchgamestore.ViewModel.userViewModel
import com.example.sketchgamestore.databinding.FragmentLoginBinding

class Login : Fragment() {
    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding

//    private lateinit var username: TextView
//    private val viewModel: userViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
//        val view = inflater.inflate(R.layout.fragment_login,container,false)
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
//        username = view.findViewById(R.id.login_username)
//        viewModel.user.observe(viewLifecycleOwner, {
//            username.text = it
//        })
//        return view
        return binding?.root
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //Go to Home
        binding?.signin?.setOnClickListener {
            val action = LoginDirections.actionLoginToHome2()
            view.findNavController().navigate(action)
        }
    }
}