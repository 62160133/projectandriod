package com.example.sketchgamestore.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.sketchgamestore.HomeDirections
import com.example.sketchgamestore.R
import com.example.sketchgamestore.datasource.DataInformationSource
import com.example.sketchgamestore.model.Game

class CardgameAdapter(private val gameList: ArrayList<Game>): RecyclerView.Adapter<CardgameAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
    val gameView = LayoutInflater.from(parent.context).inflate(R.layout.cardgame,parent, false)
        return MyViewHolder(gameView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentGame = gameList[position]
        holder.imgGame.setImageResource(currentGame.imggame)
        holder.nameGame.text = currentGame.namegame
        holder.priceGame.text = currentGame.pricegame.toString()

        //Click on RecyclerView -> InfomationGame
            holder.imgGame.setOnClickListener {
                holder.imgGame.setImageResource(DataInformationSource.datagames[position].imggame)
                var action = HomeDirections.actionHome2ToInfomationGame(position)
                holder.imgGame.findNavController().navigate(action)
        }

    }

    override fun getItemCount(): Int {
        return gameList.size
    }

    class MyViewHolder(gameView: View):RecyclerView.ViewHolder(gameView){
        val imgGame : ImageView = gameView.findViewById(R.id.img_game)
        val nameGame : TextView = gameView.findViewById(R.id.name_game)
        val priceGame : TextView = gameView.findViewById(R.id.price_game)


    }
}