package com.example.sketchgamestore.model

import androidx.annotation.DrawableRes

data class Game (val imggame: Int, val namegame:String, val pricegame:String)
