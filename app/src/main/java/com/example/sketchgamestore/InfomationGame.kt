package com.example.sketchgamestore

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.example.sketchgamestore.databinding.FragmentHomeBinding
import com.example.sketchgamestore.databinding.FragmentInfomationGameBinding
import com.example.sketchgamestore.datasource.DataInformationSource
import kotlin.reflect.KProperty


class InfomationGame : Fragment() {
    private var _binding: FragmentInfomationGameBinding? = null
    private val binding get() = _binding

    var args: InfomationGameArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentInfomationGameBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.btnSystem?.setOnClickListener {
            val action = InfomationGameDirections.actionInfomationGameToSystemRequirements()
            view.findNavController().navigate(action)
        }

        binding?.gameImage?.setImageResource(DataInformationSource.datagames[args.cardgameId].imggame)
        binding?.namegame?.text = DataInformationSource.datagames[args.cardgameId].namegame
        binding?.pricegame?.text = DataInformationSource.datagames[args.cardgameId].price
        binding?.aboutGame?.text = DataInformationSource.datagames[args.cardgameId].aboutgame
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}

private operator fun Any.setValue(infomationGame: InfomationGame, property: KProperty<*>, infomationGameArgs: InfomationGameArgs) {

}
