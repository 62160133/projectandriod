package com.example.sketchgamestore

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sketchgamestore.adapter.CardgameAdapter
import com.example.sketchgamestore.databinding.FragmentHomeBinding
import com.example.sketchgamestore.model.Game

class Home : Fragment() {
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private lateinit var adapter: CardgameAdapter
    private lateinit var recyclerView: RecyclerView
    private lateinit var gameArrayList: ArrayList<Game>

    lateinit var imageId : Array<Int>
    lateinit var nameGame : Array<String>
    lateinit var priceGame : Array<String>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dataInitialize()
        val layoutManager = LinearLayoutManager(context)
        recyclerView = view.findViewById(R.id.list)
        recyclerView.layoutManager = layoutManager
        recyclerView.setHasFixedSize(true)
        adapter = CardgameAdapter(gameArrayList)
        recyclerView.adapter = adapter

    }


    private fun dataInitialize(){
        gameArrayList = arrayListOf<Game>()
        imageId = arrayOf(
            R.drawable.apex,
            R.drawable.valorant,
            R.drawable.monsterhunter,
            R.drawable.deadbydaylight
        )
        nameGame = arrayOf(
            getString(R.string.namegame1),
            getString(R.string.namegame2),
            getString(R.string.namegame3),
            getString(R.string.namegame4)
        )

        priceGame = arrayOf(
            "Free to Play",
            "Free to Play",
            "฿ 1,200",
            "฿ 399"
        )

        for (i in imageId.indices){
            val game = Game(imageId[i],nameGame[i],priceGame[i])
            gameArrayList.add(game)
        }
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}