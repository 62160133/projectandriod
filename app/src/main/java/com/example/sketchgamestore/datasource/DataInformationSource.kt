package com.example.sketchgamestore.datasource

import com.example.sketchgamestore.R
import com.example.sketchgamestore.model.dataInfomationGame

object DataInformationSource {
    val datagames: List<dataInfomationGame> = listOf(
        dataInfomationGame(R.drawable.apex,
        "Apex Legends",
        "Free to Play",
        "   Conquer with character in Apex Legends, a free-to-play* Hero shooter where legendary characters with powerful abilities team up to battle for fame & fortune on the fringes of the Frontier Master an ever-growing roster of diverse Legends, deep-tactical squad play, and bold, new innovations that go beyond the Battle Royale experience — all within a rugged world where anything goes. Welcome to the next evolution of Hero Shooter."
        ),
        dataInfomationGame(R.drawable.valorant,
            "Valorant",
            "Free to Play",
            "   A tactical shooting game involving two teams with 5 players in each team. Every player can sign in and play remotely from anywhere in the world. Every game has 25 rounds and the team that wins 13 of them first wins the game. Players can choose their in-game characters called agents at the start of the game."
        ),
        dataInfomationGame(R.drawable.monsterhunter,
            "Monster Hunter",
            "฿ 1,200",
            "   Welcome to a new world! Take on the role of a hunter and slay ferocious monsters in a living, breathing ecosystem where you can use the landscape and its diverse inhabitants to get the upper hand. Hunt alone or in co-op with up to three other players, and use materials collected from fallen foes to craft new gear and take on even bigger, badder beasts!"
        ),
        dataInfomationGame(R.drawable.deadbydaylight,
            "Dead By Daylight",
            "฿ 399",
            "   Dead by Daylight is a multiplayer (4vs1) horror game where one player takes on the role of the savage Killer, and the other four players play as Survivors, trying to escape the Killer and avoid being caught, tortured and killed."
        ),
    )

}