package com.example.sketchgamestore.ViewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class userViewModel: ViewModel() {
    val user = MutableLiveData<String>()

    fun setUsername(newUsername : String){
        user.value = newUsername
    }
}